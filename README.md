# Introduction
PRICES (Precise Requirement Changes Integrated System) is a framework to develop
web systems based on Software Product Line Engineering (SPLE).
The development flow is designed based on model-driven software engineering (MDSE)
and delta-oriented programming (DOP).
Prices-IDE is an integration of PRICES tools in Eclipse IDE. 

There are three main phases in the development flow, modeling, implementation, 
and product generation:
## Modeling
1. UML diagram with the UML-DOP profile is used to model the web back end. 
2. Interaction flow modeling language (IFML) is used to model the abstract user interface (the web front end).

## Implementation
1. The web back end is implemented using Variability Modules for Java (VMJ).
VMJ is an architectural pattern to develop variability modules in Java, follows design patterns.
2.  The front end is implemented using JavaScript. The source code is fully generated from IFML diagram. 

## Product Generation
In SPLE, a product is generated based on selected features. In Prices-IDE,
the product generation is conducted in two steps:
1. Back end generation using FeatureIDE WinVMJ Composer
2. Front end generation using IFML UI Generator

# Prices-IDE Plugins:
Prices-IDE consists of several plugins to support the development process.
The source code is available in this group.

## Diagram Editor
We utilize the following diagram editor:
1. Papyrus Eclipse plugin to design the UML-DOP diagram
2. IFML editor to model the abstract UI using IFML diagram
3. FeatureIDE to model the feature diagram using Universal Variability Language (UVL)

## [UMl to WinVMJ (UML2Win) Generator](https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/uml-to-vmj)
The Java source code is moslty generated from UML diagram with UML-DOP profile. WinVMJ is a web framework based on VMJ. 

## [IFML UI Generator](https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/ifml-ui-generator)
IFML UI Generator transforms IFML diagram into ReactJS. JavaScript source code is fully generated
from IFML diagram based on specific templates.

## [FeatureIDE WinVMJ Composer](https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/winvmj-composer)
FeatureIDE is a framework to develop software product lines. We develop a new composer based on WiNVMJ to generate a web back end based on selected features.
